﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour {
	public Material mat;

	private void OnTriggerEnter(Collider other) {
		mat.color = Color.yellow;
	}

	private void OnTriggerExit(Collider other) {
		mat.color = Color.red;
	}
  
}
